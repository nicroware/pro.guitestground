﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NicroWare.Lib.SdlEngine;
using NicroWare.Lib.SdlGui;
using System.Xml;
using System.IO;
using System.Reflection;
using System.Diagnostics;
using NicroWare.Lib.SdlEngine.Events;

namespace NicroWare.Pro.GuiTestGround
{
    class MainGui : SDLRenderWindow
    {
        SDLRenderer renderer;
        SDLTexture squere;
        int x = 0;
        PageScroller scroller;

        SDLFont digiFont;

        Button button_clear;
        Button button_clearLight;

        private void InitializeComponent(SDLRenderer renderer)
        {
            string xmlPath = @"C:\Users\Nicolas\Desktop\CarGui.xml";
            XmlDocument document = new XmlDocument();
            document.Load(xmlPath);
            ParseRoot(renderer, document.ChildNodes);
            
            //button_clear.Click += button_clear_Click;
        }

        public override void Initialize()
        {
            renderer = SDLRenderer.Create(this);

            FileInfo fontFile = new FileInfo("Content/SFDigital.ttf");
            FileInfo font2File = new FileInfo("Content/MonospaceTypewriter.ttf");

            digiFont = SDLFont.LoadFont(fontFile.FullName, 150);
            Global.Font = SDLFont.LoadFont(font2File.FullName, 50);
            InitializeTan();

            Global.SideScroll = true;

            InitializeComponent(renderer);

            
            Location = new SDLPoint(100, 100);
            
            FrameLock = true;
            
            base.Initialize();
        }

        public void InitializeTan()
        {
            Global.TanPos = new int[451, 451];
            for (int y = 0; y < 451; y++)
            {
                for (int x = 0; x < 451; x++)
                {
                    Global.TanPos[x, y] = (int)(Math.Atan2(y - 225, x - 225) / (Math.PI * 2) * 360 + 0.5);
                }
            }
        }

        List<double> allCalcs = new List<double>();
        double a_1 = 0;
        double a_2 = -0.15;
        double a_3 = 0.8;
        double a_4 = 0;
        double b_1 = 0;
        double b_2 = 1;
        double b_3 = 1.55;
        double b_4 = 0.9;
        public byte Generator(double x, double y)
        {
            //return (byte)(((Math.Sin((x * Math.PI * 2) / 64.0) + 1) * 127.5));
            //double calculated = Math.Abs((((Math.Sin(x * a_1 / a_2) * a_3 + a_4) * (Math.Cos(x * b_1 / b_2) * b_3 + b_4)* (Math.Sin(y * a_1 / a_2) * a_3 + a_4) * (Math.Cos(y * b_1 / b_2) * b_3 + b_4)))) * 50;

            //allCalcs.Add(calculated);
            /*x = x + a_1;
            y = y + b_1;
            double f = Math.Sin((x) * Math.PI / 45.0);
            double g = Math.Sin((y) * Math.PI / 72.0);
            double h = Math.Sin((x) * Math.PI / 96.0);
            double i = Math.Sin((y) * Math.PI / 240.0);
            double j = Math.Sin((x) * Math.PI / 15.0);
            double k = Math.Sin((y) * Math.PI / 3.0);
            double f1 = Math.Sin((x - 2*y) * Math.PI / 45.0);
            double g1 = Math.Sin((x - 2*y) * Math.PI / 72.0);
            double h1 = Math.Sin((x - 2*y) * Math.PI / 96.0);
            double i1 = Math.Sin((x - 2*y) * Math.PI / 240.0);
            double j1 = Math.Sin((x - 2*y) * Math.PI / 15.0);
            double k1 = Math.Sin((x - 2*y) * Math.PI / 3.0);
            double calculated =
                f// * f 
                + g// * g 
                + h// * h 
                + i// * i 
                + j// * j 
                + k
                /*+ f1// * f1 
                + g1// * g1 
                + h1// * h1 
                + i1// * i1 
                + j1// * j1
                + k1
                ;
            calculated /= 2;
            calculated += 10;
            calculated = calculated * 10;*/
            double calculated = (Math.Sin((21 * x) % 64) * Math.Sin((47 * y) % 64) + 2) *  75;

            return (byte)calculated;
        }

        public SDLTexture GenerateTexture()
        {
            return new DrawSurface(700, 400).Initialize(ds =>
            {
                ds.CustomDraw((x, y) => ds[x, y] = new SDLColor(Generator(x, y), 200, Generator(x, y)));

            }).MakeTexture(renderer);
        }

        public override void LoadContent()
        {
            squere = GenerateTexture();

            base.LoadContent();
        }

        private void button_clear_Click(object sender, EventArgs e)
        {
            Console.WriteLine("Hello World");
        }

        public void ParseRoot(SDLRenderer renderer, XmlNodeList list)
        {
            XmlNode window = list[0];
            if (window.Name != "Window")
            {
                throw new Exception("Window node not found");
            }
            string[] winSize = (window.Attributes.GetNamedItem("Size")?.Value ?? "800;480").Split(';');
            string[] winLoc = (window.Attributes.GetNamedItem("Location")?.Value ?? "10;10").Split(';');
            string winTitle = window.Attributes.GetNamedItem("Title")?.Value ?? "Display1";
            this.Size = new SDLPoint(int.Parse(winSize[0]), int.Parse(winSize[1]));
            this.Location = new SDLPoint(int.Parse(winLoc[0]), int.Parse(winLoc[1]));
            this.Title = winTitle;
            foreach (XmlNode node in window.ChildNodes)
            {
                if (node.Name == "PageScroller")
                {
                    CreatePageScroller(renderer, node);
                }
            }
        }

        public void CreatePageScroller(SDLRenderer renderer, XmlNode node)
        {
            scroller = new PageScroller() { Location = new SDLPoint(0,0), Size = Size};
            foreach (XmlNode chld in node.ChildNodes)
            {
                if (chld.Name == "Page")
                {
                    GuiPage page = new GuiPage(renderer);
                    foreach (XmlNode ctrl in chld.ChildNodes)
                    {
                        Assembly[] allAssms = AppDomain.CurrentDomain.GetAssemblies();
                        foreach (Assembly asm in allAssms)
                        {
                            Type[] t2 = asm.GetExportedTypes();
                            foreach (Type t in t2)
                            {
                                if (t.Name == ctrl.Name && typeof(GuiElement).IsAssignableFrom(t))
                                {
                                    object temp = Activator.CreateInstance(t);
                                    foreach (XmlAttribute attr in ctrl.Attributes)
                                    {
                                        if (attr.Name == "Name")
                                        {
                                            foreach (FieldInfo fi in GetType().GetRuntimeFields())
                                            {
                                                if (fi.Name == attr.Value)
                                                {
                                                    fi.SetValue(this, temp);
                                                    break;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            PropertyInfo pi = t.GetRuntimeProperty(attr.Name);
                                            if (pi != null)
                                            {
                                                if (pi.PropertyType == typeof(SDLPoint))
                                                {
                                                    string[] point = attr.Value.Split(';');
                                                    pi.SetValue(temp, new SDLPoint(int.Parse(point[0]), int.Parse(point[1])));
                                                }
                                                else if (pi.PropertyType == typeof(int))
                                                {
                                                    pi.SetValue(temp, int.Parse(attr.Value));
                                                }
                                                else if (pi.PropertyType == typeof(bool))
                                                {
                                                    pi.SetValue(temp, bool.Parse(attr.Value));
                                                }
                                                else if (pi.PropertyType.IsEnum)
                                                {
                                                    pi.SetValue(temp, Enum.Parse(pi.PropertyType, attr.Value));
                                                }
                                                else if (pi.PropertyType == typeof(string))
                                                {
                                                    pi.SetValue(temp, attr.Value.Replace("\\n", "\n"));
                                                }
                                                else if (pi.PropertyType == typeof(SDLFont))
                                                {
                                                    foreach (FieldInfo fInfo in GetType().GetRuntimeFields())
                                                    {
                                                        if (fInfo.Name == attr.Value)
                                                        {
                                                            pi.SetValue(temp, fInfo.GetValue(this));
                                                            break;
                                                        }
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                EventInfo ei = t.GetRuntimeEvent(attr.Name);
                                                MethodInfo[] all = GetType().GetRuntimeMethods().ToArray();
                                                MethodInfo mi = null;
                                                foreach(MethodInfo minfo in all)
                                                {
                                                    if (minfo.Name == attr.Value)
                                                    {
                                                        mi = minfo;
                                                        break;
                                                    }
                                                }
                                                if (ei != null && mi != null)
                                                {
                                                    ei.AddEventHandler(temp, mi.CreateDelegate(ei.EventHandlerType, this));
                                                }
                                            }
                                        }
                                    }
                                    page.Elements.Add(temp as GuiElement);
                                    break;
                                }
                            }
                        }
                    }
                    page.Initialize(renderer);
                    scroller.AllPages.Add(page);
                }
            }
        }

        public void UpdateKeys(KeyboardState state)
        {
            double step = 0.1;
            if (state.IsKeyDown(Keys.Q))
                a_1 -= step;
            if (state.IsKeyDown(Keys.W))
                a_2 -= step;
            if (state.IsKeyDown(Keys.E))
                a_3 -= step;
            if (state.IsKeyDown(Keys.R))
                a_4 -= step;
            if (state.IsKeyDown(Keys.T))
                b_1 -= step;
            if (state.IsKeyDown(Keys.Y))
                b_2 -= step;
            if (state.IsKeyDown(Keys.U))
                b_3 -= step;
            if (state.IsKeyDown(Keys.I))
                b_4 -= step;
            if (state.IsKeyDown(Keys.A))
                a_1 += step;
            if (state.IsKeyDown(Keys.S))
                a_2 += step;
            if (state.IsKeyDown(Keys.D))
                a_3 += step;
            if (state.IsKeyDown(Keys.F))
                a_4 += step;
            if (state.IsKeyDown(Keys.G))
                b_1 += step;
            if (state.IsKeyDown(Keys.H))
                b_2 += step;
            if (state.IsKeyDown(Keys.J))
                b_3 += step;
            if (state.IsKeyDown(Keys.K))
                b_4 += step;
            Console.WriteLine("a_1: " + a_1 + " b_1: " + b_1);
            squere.Dispose();
            squere = GenerateTexture();
            GC.Collect();
        }

        KeyboardState current;
        KeyboardState prev;

        public override void Update(GameTime gameTime)
        {
            //page.Update(gameTime);
            scroller.Update(gameTime);
            prev = current;
            current = Keyboard.GetState();

            if (current.IsKeyDown(Keys.F5) && prev.IsKeyUp(Keys.F5))
            {
                int currentPage = scroller.CurrentPage;
                InitializeComponent(renderer);
                scroller.CurrentPage = currentPage;
            }

        }

        public override void Draw(GameTime gameTime)
        {
            renderer.Clear();
            //renderer.DrawPoint(5, 5, SDLColor.Red);
            
            scroller.Draw(renderer, gameTime);
            //renderer.DrawTexture(squere, new SDLRectangle(x, 10, squere.Width, squere.Height));
            //page.Draw(renderer, gameTime);
            renderer.Present();
        }
    }
}
